ROPE_LENGTH = 4; // the equilibirum length of each rope
ROPE_LENGTH_SQ = ROPE_LENGTH * ROPE_LENGTH; // square of the rope length
ROPE_SPACING = 0.2; // initial horizontal distance between vertices
ROPE_MASS = 5; // Mass per length of rope (actually the mass of vertices)
X_OFFSET = 100;
Y_OFFSET = 20;
POS_SCALE = 40;
STRING_VERTEX_DAMPING = 0;
var ZOOM = 80;
var graph = {
	speed: 30, // pixels per second on graph
	rate: 20, // samples per second
	nodes: [],
	last: 0,
	time: 0,
	play: true
};

var colors = {
	ball: ["silver", "black"],
	rope: ["white", "black"],
	graph: ["white", "black"],
	velocity: ["#0ff", "blue"],
	acc: ["red", "red"]
}

// Start
function init()
{
	createVertex(0, 0, false);
	createVertex(0, 1, false);
	points[0].fixed = true;
	points[1].mass = 1 / 9.81;
	joinVertices(0, 1);
	POS_SCALE = ($(window).height()) / 15;

	engine.onMouseDown = function(e) {
		var height = 2 * engine.ce.height / 3
		var top = engine.ce.height /2 - height / 2;
		var right = engine.ce.width - 100;
		var pos = posToScreen(0, points[1].y);
		var mpos = mousePosition();
		if (mpos.x > right - 50 && mpos.x < right + 50 && mpos.y < engine.ce.height - 50)
			graph.selected = true;
	}

	engine.onMouseUp = function(e) {
		graph.selected = false;
	}

	$("#pauseplay").click(function() {
		if (graph.play) {
			$("#pauseplay").text("Play");
			graph.play = false;
		} else {
			$("#pauseplay").text("Pause");
			graph.play = true;
		}
	});

	$("#clear").click(function() {
		graph.nodes = [];
	});
}

function tick(ce, c, dtime)
{
	// Limit dtime
	if (dtime > 0.2)
		dtime = 0.1;

	if (graph.play)
		graph.time += dtime;

	STRING_VERTEX_DAMPING = $("#damping").val();
	var sc = $("#springconstant").val();
	var mass = $("#mass").val();
	if (sc && sc > 0 && !isNaN(sc))
		ropes[0].spring_const = sc;
	if (mass && mass > 0 && !isNaN(mass))
		points[1].mass = mass;

	if (graph.time > graph.last + 1 / graph.rate) {
		graph.last = graph.time;
		var a = getAcceleration(1, 1);
		graph.nodes.push({x: graph.time, y: points[1].y, v: points[1].v.y, a:a.y});
	}

	var equil = 4 + (9.81 * points[1].mass) / ropes[0].spring_const;
	var height = ce.height * 8/10;
	POS_SCALE = ce.height / (equil * 2) * 0.8;
	var top = 0;
	var right = ce.width - 100;

	if (graph.selected) {
		points[1].v.y = 0;
		points[1].y = screenToPos(0, mousePosition().y - top).y;
		if (points[1].y < 0.1)
			points[1].y = 0.1;
	}

	// Do physics
	if (graph.play) {
		for (var i = 0; i < 10; i++) {
			resolve(dtime / 5);
		}
		points[1].x = 0;
		points[1].v.x = 0;

		if (points[1].y < 0.1) {
			points[1].y = 0.1;
			points[1].v.y *= -0.1;
		}
	}
	// Draw ropes
	for (var i = 0; i < ropes.length; i++) {
		var rope = ropes[i];
		var one  = points[rope.one];
		var two  = points[rope.two];
		var onep = posToScreen(0, one.y);
		var twop = posToScreen(0, two.y);
		c.beginPath();
		c.strokeStyle = color("rope");
		c.moveTo(right, top + onep.y + 0.5);
		c.lineTo(right, top + twop.y + 0.5);
		c.lineWidth = 3;
		c.stroke();
	}
	var pos = posToScreen(points[1].x, points[1].y);
	c.beginPath();
	c.arc(right, top + pos.y, 10, 0, 2 * Math.PI, false);
	c.fillStyle = color("ball");
	c.fill();
	graph.speed = 60 * $("#zoom").val() / 100;
	if ($("#x > input").is(":checked")) {
		c.beginPath();
		c.strokeStyle = color("graph");
		c.lineWidth = 1;
		for (var i = 0; i < graph.nodes.length; i++) {
			var node = graph.nodes[i];
			var npos = posToScreen(0, node.y);
			npos.x = right + (node.x - graph.time) * graph.speed;
			npos.y += top;

			if (npos.x < -10) {
				graph.nodes.splice(i, 1);
				i--;
			} else if (i == 0)
				c.moveTo(npos.x, npos.y);
			else
				c.lineTo(npos.x, npos.y);
		}
		c.stroke();
	}
	if ($("#v > input").is(":checked")) {
		c.beginPath();
		c.strokeStyle = color("velocity");
		c.lineWidth = 1;
		for (var i = 0; i < graph.nodes.length; i++) {
			var node = graph.nodes[i];
			var npos = posToScreen(0, equil + node.v * 0.3);
			npos.x = right + (node.x - graph.time) * graph.speed;
			npos.y += top;

			if (npos.x < -10) {
				graph.nodes.splice(i, 1);
				i--;
			} else if (i == 0)
				c.moveTo(npos.x, npos.y);
			else
				c.lineTo(npos.x, npos.y);
		}
		c.stroke();
	}
	if ($("#a > input").is(":checked")) {
		c.beginPath();
		c.strokeStyle = color("acc");
		c.lineWidth = 1;
		for (var i = 0; i < graph.nodes.length; i++) {
			var node = graph.nodes[i];
			var npos = posToScreen(0, equil + node.a * 0.1);
			npos.x = right + (node.x - graph.time) * graph.speed;
			npos.y += top;

			if (npos.x < -10) {
				graph.nodes.splice(i, 1);
				i--;
			} else if (i == 0)
				c.moveTo(npos.x, npos.y);
			else
				c.lineTo(npos.x, npos.y);
		}
		c.stroke();
	}
}
