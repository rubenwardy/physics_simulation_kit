var game = {
	fps: {
		dtime: 0,
		rtime: 0,
		utime: 0,
		last_tick: 0,
		target: 65
	},
	circles: []
};


// Start
function init(){
	game.ce = document.getElementById("canvas");
	game.c = game.ce.getContext("2d");
	game.ce.width = $(window).width();
	game.ce.height = $(window).height();
	setInterval(tick,1000/65);
	$(window)[0].addEventListener('mousemove', function(e) {
		game.ms = {
		  x: e.clientX-5,
		  y: e.clientY-5
		};
	});
	$(window)[0].addEventListener('mousedown', function(e){
		game.md = true;
	}, false);
	$(window)[0].addEventListener('mouseup', function(e){
		game.md = false;
	}, false);
	setInterval(function(){
		if (game.md){
			var colors = ["red","green","blue","yellow"];
			game.circles.push({x:game.ms.x,y:game.ms.y,radius:2.0,color:colors[Math.floor(Math.random()*colors.length)]});
		}
	},1000/15);
}

function tick(){
	game.ce.width = $(window).width();
	game.ce.height = $(window).height();

	while (game.circles.length > 2000) {
		game.circles.splice(0, 1);
	}
	
	var dtime = game.fps.dtime / 1000;
	if (dtime > 0.01)
		dtime = 0.01;

	for (var i=0; i < game.circles.length; i++){
		var circle = game.circles[i];
		game.c.beginPath();
		game.c.arc(circle.x, circle.y, circle.radius, 0, 2 * Math.PI, false);
		game.c.lineWidth = 1;
		game.c.strokeStyle = circle.color;
		game.c.stroke();
		circle.radius += 200 * dtime;
	}

	// Time settings
	var end = new Date().getTime();
	game.fps.dtime = Math.round(end - game.fps.last_tick);
	if (game.fps.dtime > 1000)
		game.fps.dtime = 1000;
	game.fps.last_tick = end;
}

$(init);
