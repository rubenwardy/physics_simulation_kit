// Graph library, by rubenwardy
// License: LGPL 3.0 or later

function Graph(xaxis, yaxis) {
	this.xaxis = xaxis;
	this.yaxis = yaxis;
	this.points = [];
	this.line = null;
}

Graph.prototype.plot = function(point) {
	this.points.push(point);
	this.generateLOBF();
};

Graph.prototype.pointToScreen = function(pt, x, y, w, h) {
	var xp = pt[this.xaxis.name];
	var yp = pt[this.yaxis.name];
	return this.posToScreen({x: xp, y: yp}, x, y, w, h);
}

Graph.prototype.posToScreen = function(pos, x, y, w, h) {
	return {
		x: x + 30.5     + (pos.x * (w-60)/(this.xaxis.maxval - this.xaxis.minval)),
		y: y - 30.5 + h - (pos.y * (h-40)/(this.yaxis.maxval - this.yaxis.minval))
	};
}

Graph.prototype.screenToPos = function(pos, x, y, w, h) {
	return {
		x: ((pos.x - x - 30.5) * (this.xaxis.maxval - this.xaxis.minval)) / (w-60),
		y: NaN //y - 30.5 + h - (pos.y * (h-40)/(this.yaxis.maxval - this.yaxis.minval))
	};
}

Graph.prototype.swapAxis = function() {
	var tmp = this.yaxis;
	this.yaxis = this.xaxis;
	this.xaxis = tmp;
}

Graph.prototype.setAxis = function(x, y) {
	if (this.xaxis.name == x) {
		assert(this.yaxis.name == y, "Axis called " + y + " does not exist!");
		return;
	} else {
		assert(this.xaxis.name == y, "Axis called " + y + " does not exist!");
		assert(this.yaxis.name == x, "Axis called " + x + " does not exist!");
		this.swapAxis();
	}
}

Graph.prototype.drawLine = function(canvas, m, c, x, y, w, h) {
	canvas.strokeStyle = "white";
	var start = {x: 0, y: c};
	if (c < 0) {
		start = {x: c/m, y: 0};
	}
	var end = {x: this.xaxis.maxval, y: this.xaxis.maxval * m + c};
	start = this.posToScreen(start, x, y, w, h);
	end = this.posToScreen(end, x, y, w, h);
	canvas.beginPath();
	canvas.moveTo(start.x, start.y);
	canvas.lineTo(end.x,   end.y);
	canvas.lineWidth = 1;
	canvas.stroke();
}

Graph.prototype.generateLOBF = function() {
	var n = this.points.length;
	if (n == 0) {
		this.line = null;
		return;
	}
	var zx = 0;
	var zy = 0;
	var zx2 = 0;
	var zxy = 0;
	for (var i = 0; i < this.points.length; i++) {
		zx += this.points[i][this.xaxis.name];
		zy += this.points[i][this.yaxis.name];
		zx2 += Math.pow(this.points[i][this.xaxis.name], 2);
		zxy += this.points[i][this.xaxis.name] * this.points[i][this.yaxis.name];
	}
	var sxx = zx2 - Math.pow(zx, 2) / n;
	var sxy = zxy - (zx * zy) / n;
	var b = sxy / sxx;
	var a = zy / n - b * zx / n;
	this.line = {m: b, c: a};
}

Graph.prototype.drawAxis = function(c, x, y, w, h) {
	// Draw axis
	c.strokeStyle = "white";
	c.beginPath();
	c.moveTo(x + 30.5, y + 0.5);
	c.lineTo(x + 30.5, y + h - 30.5);
	c.lineTo(x + w - 20.5, y + h - 30.5);
	c.lineWidth = 1;
	c.stroke();

	// Set up properties
	c.beginPath();
	c.strokeStyle = "#444";
	c.textAlign = 'center';
	c.fillStyle = 'white';

	// Draw axis labels
	c.font = '10pt Arial';
	this.drawXaxis(c, x, y, w, h);
	this.drawYaxis(c, x, y, w, h);
	c.stroke();
	c.font = '14pt Arial';
	c.fillText(this.xaxis.title + " / " + this.xaxis.unit, x + w - 120, y + h + 20);
	c.fillText(this.yaxis.title + " / " + this.yaxis.unit, x + 100, y + 30);

	// Draw line
	if (this.line) {
		this.drawLine(c, this.line.m, this.line.c, x, y, w, h);
		var mouse = mousePosition();
		mouse.x -= CANVAS_H_MARGIN;
		if (mouse.x > x && mouse.x < x + w && mouse.y > y && mouse.y < y + h) {
			c.beginPath();
			c.strokeStyle = "white";
			c.lineWidth = 1;
			c.moveTo(Math.floor(mouse.x) + 0.5, Math.floor(y + h) - 30);
			var lx = this.screenToPos({x: mouse.x, y: 0}, x, y, w, h).x;
			var to = this.posToScreen({x: lx, y: this.line.m * lx + this.line.c}, x, y, w, h);
			console.log(lx + " : " + to.x + ", " + to.y);
			c.lineTo(Math.floor(to.x) + 0.5, Math.floor(to.y));
			c.stroke();
			c.fillStyle = "white";
			c.font = "10pt Arial";
			c.textAlign = "left"
			c.fillText("(" + Math.round(lx * 100)/100 + ", " + Math.round((this.line.m * lx + this.line.c) * 100)/100 + ")", to.x + 10, to.y + 10);
		}
	}

	// Stroke
	c.font = '10pt Arial';
	c.textAlign = "left";
	for (var i = 0; i < this.points.length; i++) {
		var pt = this.points[i];
		var xp = pt[this.xaxis.name];
		var yp = pt[this.yaxis.name];
		c.beginPath();
		var pos = this.pointToScreen(pt, x, y, w, h);
		c.arc(pos.x, pos.y, 5, 0, 2 * Math.PI, false);
		c.fill();
		c.fillText("(" + to2dp(xp) + ", " + to2dp(yp) + ")", pos.x + 20, pos.y + 20);
	}
};

Graph.prototype.drawXaxis = function(c, x, y, w, h) {
	var no = this.xaxis.maxval / this.xaxis.step;
	var xspread = (w-60) / no;

	for (var ix = 0; ix <= no; ix++){
		var ax = Math.round(x + ix * xspread) + 30.5;
		c.fillText(this.xaxis.minval + (ix * this.xaxis.step), ax,  y + h - 10.5);
		if (ix != 0) {
			c.moveTo(ax,  y + 0.5);
			c.lineTo(ax,  y + h - 31.5);
		}
	}
};

Graph.prototype.drawYaxis = function(c, x, y, w, h) {
	var no = this.yaxis.maxval / this.yaxis.step;
	var yspread = (h-40) / no;
	for (var iy = 0; iy <= no; iy++){
		var ay = Math.round(y + h - iy * yspread) - 30.5;
		c.fillText(this.yaxis.minval + (iy * this.yaxis.step), x + 10.5, ay);
		if (iy != 0) {
			c.moveTo(x + 31.5, ay);
			c.lineTo(x + w - 20.5, ay);
		}
	}
};

