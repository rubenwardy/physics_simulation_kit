var game = {
	fps: {
		dtime: 0,
		rtime: 0,
		utime: 0,
		last_tick: 0,
		target: 65
	},
	circles: []
};


// Start
function init()
{
	$(window)[0].addEventListener('mousedown', function(e){
		game.md = true;
	}, false);
	$(window)[0].addEventListener('mouseup', function(e){
		game.md = false;
	}, false);
	setInterval(function(){
		if (game.md){
			var mouse = mousePosition();
			game.circles.push({x: mouse.x, y: mouse.y, radius: 2.0});
		}
	}, 1000/3);
}

function tick(ce, c, dtime)
{
	while (game.circles.length > 200) {
		game.circles.splice(0, 1);
	}
	
	if (dtime > 0.01)
		dtime = 0.01;

	for (var i=0; i<game.circles.length; i++){
		var circle = game.circles[i];
		c.beginPath();
		c.arc(circle.x, circle.y, circle.radius, 0, 2 * Math.PI, false);
		c.lineWidth = 1;
		c.strokeStyle = 'white';
		c.stroke();
		circle.radius += 200 * dtime;
	}
}

$(init);
