var MET_SC = 50; // metre scale
var VEC_SC = 2.5
var STATE_SETUP = 1;
var STATE_RUN = 2;
var STATE_PAUSE = 3;
var STATE_STOP = 4;
var world = {};

function updateState()
{
	if (world.state == STATE_RUN) {
		$("#start").text("Pause");
		$("#start_element").show();
	} else if (world.state == STATE_SETUP || world.state == STATE_PAUSE) {
		$("#start").text("Start");
		$("#start_element").show();
	} else {
		$("#start_element").hide();
	}
}

function reset()
{
	world = {
		ball: {x: 0, y: 0, v: {x: 5, y: 12}},
		initial: {x: 0, y: 0, v: {x: 6, y: 10}},
		last_x: 0,
		state: STATE_SETUP,
		selected: -1,
		line: [],
		line_last: 0
	};
	updateState();
}

function restart()
{
	world.state = STATE_SETUP;
	world.ball = world.initial;
	world.initial = {x: world.ball.x, y: world.ball.y, v: {x: world.ball.v.x, y: world.ball.v.y}};
	updateState();
}

var colors = {
	ball_bg: ["white", "black"],
	vector: ["white", "black"],
	text: ["#e0e0e0", "black"],
	bg: ["black", "white"],
	vector_end: ["white", "black"],
	grid: ["#666666", "black"],
	grid_line: ["#666666", "black"],
	grid_line_text: ["#666666", "black"],
	trace: ["#999", "black"],
	trace_balls: ["rgba(100, 100, 100,0.6)", "black"],
	ref_line: ["#ccc", "black"],
	ref_text: ["#ccc", "black"]
}

function pToScr(x, y)
{
	return {x: (x + 0.5) * MET_SC + 100, y: ($(window).height() - 200) - y * MET_SC};
}

function scrToP(x, y)
{
	return {x: (x - 100) / MET_SC - 0.5, y: ($(window).height() - y - 200) / MET_SC};
}

function dist(one, two)
{
	return Math.pow(one.x - two.x, 2) + Math.pow(one.y - two.y, 2);
}

function init() {
	reset();
	updateSetup();
	engine.onMouseDown = function(e) {
		if (world.state == STATE_SETUP) {
			if (dist(engine.mouse, pToScr(world.ball.x, world.ball.y + 0.5)) < 1000) {
				world.selected = 0;
			}
			if (dist(engine.mouse, pToScr(world.ball.x + world.ball.v.x / VEC_SC,
					world.ball.y+world.ball.v.y / VEC_SC + 0.5)) < 100) {
				world.selected = 1;
			}
		}
	};
	engine.onMouseUp = function(e) {
		world.selected = -1;
	};
	$(".start").click(function() {
		if (world.state == STATE_SETUP || world.state == STATE_PAUSE) {
			if (world.state == STATE_SETUP)
				world.line = [];
			world.state = STATE_RUN;
			world.line.push({x: world.ball.x, y: world.ball.y, v: {x: world.ball.v.x, y: world.ball.v.y}});
			world.line_last = new Date().getTime();
		} else if (world.state == STATE_RUN) {
			world.state = STATE_PAUSE;
		}
		updateState();
	});
	$("#clear").click(reset);
	$("#restart").click(restart);
	$("#reset").click(reset);
}

function update(dtime) {

	if (world.selected != -1) {
		if (world.selected == 0) {
			var pos = scrToP(engine.mouse.x, engine.mouse.y + MET_SC / 2);
			world.ball.x = Math.floor(pos.x * 10) / 10;
			world.ball.y = Math.floor(pos.y * 10) / 10;
			if (world.ball.y < 0)
				world.ball.y = 0;
			if (world.ball.x < 0)
				world.ball.x = 0;
			world.initial = {x: world.ball.x, y: world.ball.y, v: {x: world.ball.v.x, y: world.ball.v.y}};
		}
		if (world.selected == 1) {
			var pos = scrToP(engine.mouse.x, engine.mouse.y + MET_SC / 2);
			world.ball.v.x = VEC_SC * Math.floor((pos.x - world.ball.x) * 10) / 10;
			world.ball.v.y = VEC_SC * Math.floor((pos.y - world.ball.y) * 10) / 10;
			world.initial = {x: world.ball.x, y: world.ball.y, v: {x: world.ball.v.x, y: world.ball.v.y}};
			updateSetup();
		}
	}

	if (world.state == STATE_RUN) {
		world.ball.x += world.ball.v.x * dtime;
		world.ball.y += world.ball.v.y * dtime;
		world.ball.v.y -= 9.81 * dtime;

		if (world.ball.y < -10) {
			console.log("Stopped");
			world.state = STATE_STOP;
			updateState();
		}

		if (new Date().getTime() > world.line_last + 50) {
			world.line.push({x: world.ball.x, y: world.ball.y, v: {x: world.ball.v.x, y: world.ball.v.y}});
			world.line_last = new Date().getTime();
		}
	}

	// Draw grid line
	if (world.ball.y >= 0) {
		this.line_at = world.ball.x;
	}
}

function to4dp(input)
{
	return Math.round(input * 1000) / 1000;
}

function updateSetupComp()
{
	$("#velox").val(to4dp(world.ball.v.x));
	$("#veloy").val(to4dp(world.ball.v.y));
}

function updateSetupAngle()
{
	$("#speed").val(to4dp(Math.sqrt(Math.pow(world.ball.v.x, 2) + Math.pow(world.ball.v.y, 2))));
	var angle = - 180 * Math.atan(-world.ball.v.y / world.ball.v.x) / Math.PI;
	$("#angle").val(to4dp(angle));
}

function updateSetup()
{
	updateSetupComp();
	updateSetupAngle();
}

function checkSetup()
{
	if (world.state != STATE_SETUP)
		return;

	var velox = parseFloat($("#velox").val());
	var veloy = parseFloat($("#veloy").val());
	var speed = parseFloat($("#speed").val());
	var angle = parseFloat($("#angle").val());

	console.log(velox + " - " + typeof(velox))

	if (!isNaN(angle)  && !isNaN(speed) &&
			(
				speed != to4dp(Math.sqrt(Math.pow(world.ball.v.x, 2) + Math.pow(world.ball.v.y, 2))) ||
				angle != to4dp(- 180 * Math.atan(-world.ball.v.y / world.ball.v.x) / Math.PI)
			)) {
		world.ball.v.x = speed * Math.cos(angle * Math.PI / 180);
		world.ball.v.y = speed * Math.sin(angle * Math.PI / 180);
		updateSetupComp();
		return;
	}

	if (!isNaN(velox) && velox != to4dp(world.ball.v.x)) {
		world.ball.v.x = velox;
		updateSetupAngle();
	}

	if (!isNaN(veloy) && veloy != to4dp(world.ball.v.y)) {
		world.ball.v.y = veloy;
		updateSetupAngle();
	}
}

function tick(ce, c, dtime){
	update(dtime);
	checkSetup();
	var pos = pToScr(world.ball.x, world.ball.y);

	if (world.state == STATE_SETUP && MODE == 0) {
		$("#setup").show();
		$("#setup").css("top", ce.height - 165);
		$("#setup").css("left", pos.x - 80);
	} else {
		$("#setup").hide();
	}

	// Draw grid
	var gh = ce.height - 200.5;
	c.strokeStyle = color("grid");
	c.beginPath();
	c.moveTo(0, gh);
	c.lineTo(ce.width, gh);
	c.stroke();

	// Draw grid labels
	var x = 0;
	c.fillStyle = color("text");
	while (1) {
		var l = pToScr(x, 0);
		if (l.x > ce.width - 100)
			break;

		c.fillText(x + "m", l.x, ce.height - 180);
		x++;
	}

	var lp = pToScr(this.line_at, 0);
	c.strokeStyle = color("grid_line");
	c.beginPath();
	c.moveTo(Math.floor(lp.x) + .5, gh - 5.5);
	c.lineTo(Math.floor(lp.x) + .5, gh + 5.5);
	c.stroke();
	c.fillStyle = color("grid_line_text");
	c.fillText(Math.floor(this.line_at*10)/10, Math.floor(lp.x) - 7, gh - 10);

	c.strokeStyle = color("trace");
	c.beginPath();
	for (var i = 0; i < world.line.length; i++) {
		var lpos = pToScr(world.line[i].x, world.line[i].y);
		if (i == 0)
			c.moveTo(lpos.x, lpos.y);
		else
			c.lineTo(lpos.x, lpos.y);
	}
	c.stroke();
	c.fillStyle = color("trace_balls");
	for (var i = 0; i < world.line.length; i++) {
		var lpos = pToScr(world.line[i].x, world.line[i].y);
		if (world.line[i].y > 0) {
			c.beginPath();
			if (world.line[i].v.y > 0)
				c.arc(85, lpos.y, 5, 0, 2 * Math.PI, false);
			else
				c.arc(100, lpos.y, 5, 0, 2 * Math.PI, false);
			c.fill();
			c.beginPath();
			c.arc(lpos.x, 15, 5, 0, 2 * Math.PI, false);
			c.fill();
		}
	}

	// Draw vector line
	var vec = pToScr(world.ball.x + world.ball.v.x / VEC_SC, world.ball.y + world.ball.v.y / VEC_SC);
	c.strokeStyle = color("vector");
	c.beginPath();
	c.moveTo(pos.x, pos.y - MET_SC / 2);
	c.lineTo(vec.x, vec.y - MET_SC / 2);
	c.stroke();

	// Draw Ball
	c.fillStyle = color("ball_bg");
	c.beginPath();
	c.arc(pos.x, pos.y - MET_SC/2, MET_SC/2, 0, 2 * Math.PI, false);
	c.fill();

	// Draw vector handle
	if (world.state == STATE_SETUP) {
		c.fillStyle = color("vector_end");
		c.beginPath();
		c.arc(vec.x, vec.y - MET_SC / 2, 10, 0, 2 * Math.PI, false);
		c.fill();
	}

	// Draw vector label
	//var force = Math.floor(Math.sqrt(Math.pow(world.ball.v.x, 2) + Math.pow(world.ball.v.y, 2)) * 100) / 100;
	//var angle = - Math.floor(1800 * Math.atan(-world.ball.v.y / world.ball.v.x) / Math.PI) / 10;
	//c.fillStyle = color("text");
	//c.fillText(force + " ms-1 at "+angle+" deg", vec.x - 25, vec.y - 20 - MET_SC / 2);

	// Draw reference
	if (world.line && world.line.length > 0) {
		var mouse = mousePosition();
		var pos = scrToP(mouse.x, mouse.y);
		var ry = -100;
		for (var i = 0; i < world.line.length; i++) {
			var vert = world.line[i];
			if (vert.x >= pos.x) {
				if (i != 0) {
					var b4 = world.line[i - 1];
					ry = (pos.x - b4.x) / (vert.x - b4.x)  *  (vert.y - b4.y) + b4.y;
				}
				break;
			}
		}

		if (ry > -100) {
			console.log("drawing");
			var ay = pToScr(0, ry).y;
			c.strokeStyle = color("ref_line");
			c.beginPath();
			c.moveTo(Math.floor(mouse.x) + 0.5, ce.height - 200);
			c.lineTo(Math.floor(mouse.x) + 0.5, ay);
			c.stroke();

			c.fillStyle = color("ref_text");
			c.fillText(Math.round(ry * 100) / 100 + "m", mouse.x + 10, ce.height - 220);
		}
	}
}
