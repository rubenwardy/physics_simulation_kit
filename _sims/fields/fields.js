var sim = {
	particles: [],
	fields: [{x: 3, y: 2.3, type: "magnetic", value: "into", tesla: Math.pow(10, -3)}],
	spawn_count: 10000
};

var colors = {
	erase: ["rgba(0, 0, 0, 0.03)", "rgba(255, 255, 255, 0.03)"],
	particle: ["rgba(0, 0, 255, 0.8)", "rgba(0, 0, 255, 0.8)"],
	particle_pos: ["rgba(255, 0, 0, 0.8)", "rgba(255, 0, 0, 0.8)"],
	field: ["white", "black"],
	field_marking: ["black", "white"]
};

function init() {
	sim.ce = document.getElementById("particles");
	sim.ce.width = $(window).width();
	sim.ce.height = $(window).height();
	sim.c = sim.ce.getContext("2d");
	sim.c.clearRect(0, 0, sim.ce.width, sim.ce.height);

	$("#toggletheme").click(function() {
		setTimeout(function(){
			if (MODE)
				sim.c.fillStyle = "white";
			else
				sim.c.fillStyle = "black";
			sim.c.fillRect(0, 0, sim.ce.width, sim.ce.height);
		}, 10);

	});

	// setInterval(function() {
	// 	tick(1/65);
	// }, 1000/65);
}

// $(init);

// Bqv = 1/2mv2 / r

var MET_SC = 100;

function resolveForce(x, y, v, charge) {
	var force = {x: 0, y: 0};
	for (var i = 0; i < sim.fields.length; i++) {
		var field  = sim.fields[i];

		// Get velocity perpendicular to field
		var velocity = (Math.sqrt(Math.pow(v.x, 2) + Math.pow(v.y, 2)));// * Math.cos(Math.atan2(x - field.x, y - field.y) + Math.PI);

		// Calculate the magnitude of the force
		//var radius = Math.sqrt(Math.pow(x - field.x, 2) + Math.pow(y - field.y, 2));
		var res    = Math.abs(charge) * velocity * field.tesla;// / Math.pow(radius, 2);
		// ^    Q * v * B

		// Angle of conventional current
		var ang = Math.atan2(v.y, v.x);
		if (charge < 0)
			ang -= Math.PI; // Adjust angle for electons

		// Get perpendicular angle
		if (field.value == "into")
			ang += Math.PI / 2;
		else
			ang -= Math.PI / 2;

		// Calculate force
		force.x += res * -Math.cos(ang);
		force.y += res * -Math.sin(ang);
	}
	return force;
}

function tick(ce, c, dtime) {
	sim.spawn_count += dtime;
	if (sim.spawn_count > 0.3) {
		sim.spawn_count = 0;
		sim.toggle = !sim.toggle;
		if (sim.toggle)
			sim.particles.push({ x: 2.5, y: 3, v: {x: 6, y: 0}, charge: 1 });
		else
			sim.particles.push({ x: 2.5, y: 3, v: {x: 1, y: 0}, charge: -1 });
	}

	while (sim.particles.length > 200) {
		sim.particles.splice(0, 1);
	}

	sim.c.fillStyle = color("erase");
	sim.c.fillRect(0, 0, sim.ce.width, sim.ce.height);

	sim.c.lineWidth = 3;
	for (var i = 0; i < sim.particles.length; i++) {
		var part = sim.particles[i];
		if (part.charge > 0)
			sim.c.strokeStyle = color("particle_pos");
		else
			sim.c.strokeStyle = color("particle");

		sim.c.beginPath();
		sim.c.moveTo(part.x * MET_SC, part.y * MET_SC);
		part.x += part.v.x * dtime;
		part.y += part.v.y * dtime;
		sim.c.lineTo(part.x * MET_SC, part.y * MET_SC);
		sim.c.stroke();

		var force = resolveForce(part.x, part.y, part.v, part.charge);
		part.v.x += (force.x / (0.00001) * dtime) / 100;
		part.v.y += (force.y / (0.00001) * dtime) / 100;
		part.v.x *= 0.997;
		part.v.y *= 0.997;
	}

	for (var i = 0; i < sim.fields.length; i++) {
		var field = sim.fields[i];

		c.beginPath();
		c.arc(field.x * MET_SC, field.y * MET_SC, 20, 0, 2*Math.PI, false);
		c.fillStyle = color("field");
		c.fill();

		if (field.value == "into") {
			c.beginPath();
			c.lineWidth = 5;
			c.strokeStyle = color("field_marking");
			c.moveTo(field.x * MET_SC - 10, field.y * MET_SC - 10);
			c.lineTo(field.x * MET_SC + 10, field.y * MET_SC + 10);
			c.moveTo(field.x * MET_SC - 10, field.y * MET_SC + 10);
			c.lineTo(field.x * MET_SC + 10, field.y * MET_SC - 10);
			c.stroke();
		} else {
			c.beginPath();
			c.arc(field.x * MET_SC, field.y * MET_SC, 5, 0, 2*Math.PI, false);
			c.fillStyle = color("field_marking");
			c.fill();
		}
	}
}
