var game = {
	fps: {
		dtime: 0,
		rtime: 0,
		utime: 0,
		last_tick: 0,
		target: 65
	},
	one: {x: 100, y: 200, shift: 0},
	two: {x: 100, y: 400, shift: 0},
	selected: null,
	path: null
};

CANVAS_V_MARGIN = 38;

// Start
function init()
{
	game.ce = engine.ce;
	game.c = engine.c;

	engine.onCanvasMouseDown = function(e) {
		if (e.which != 1)
			return;
	
		game.mousedown = true;
		
		function distance_sq(x, y, a, b){
			return Math.pow(x-a,2) + Math.pow(y-b,2);
		}
		game.selected = null;
		
		var mouse = mousePosition();
		if (distance_sq(mouse.x, mouse.y, game.one.x, game.one.y) < 100){
			game.selected = 1;
		}else if (distance_sq(mouse.x, mouse.y, game.two.x, game.two.y) < 100){
			game.selected = 2;
		}else if (mouse.y < game.ce.height){
			game.path = mouse;
		}
	};
	engine.onMouseUp = function(e) {
		if (e.which != 1)
			return;
		game.mousedown = false;
	};
	
	setInterval(tick,1000/65);
}

function drawSource(x, y, shift, line_color)
{
	game.c.beginPath();
	game.c.arc(x, y, 10, 0, 2 * Math.PI, false);
	game.c.fillStyle = color("pts");
	game.c.fill();
	
	for (var i=shift%30+10; i<$(window).width()+100; i+=30){
		game.c.beginPath();
		game.c.arc(x, y, i, 0, 2 * Math.PI, false);
		game.c.lineWidth = 1;
		game.c.strokeStyle = line_color;
		game.c.stroke();
	}
}

var colors = {
	waves: ["#999", "black"],
	waves_when_path: ["#666", "black"],
	pts: ["white", "black"],
	pd: ["white", "black"]
};

function tick(ce, c, dtime)
{
	game.one.shift = 30 * Math.abs(Number($('#one_shift').val()))/360;
	game.two.shift = 30 * Math.abs(Number($('#two_shift').val()))/360;
	
	// Draw sources
	var wcolor = color("waves");
	if (game.path)
		wcolor = color("waves_when_path");
	drawSource(game.one.x, game.one.y, game.one.shift, wcolor);
	drawSource(game.two.x, game.two.y, game.two.shift, wcolor);
	if (game.path){
		var dist_one = Number(
			Math.sqrt(Math.pow(game.path.x - game.one.x, 2) +
			Math.pow(game.path.y - game.one.y, 2))
			- 10 - game.one.shift % 30
		);
		var dist_two = Number(
			Math.sqrt(Math.pow(game.path.x - game.two.x, 2) +
			Math.pow(game.path.y - game.two.y, 2))
			- 10 - game.two.shift % 30
		);
		var path_diff = Math.abs((dist_one - dist_two) / 30);
	
		game.c.beginPath();
		game.c.moveTo(game.one.x+0.5, game.one.y+0.5);
		game.c.lineTo(game.path.x+0.5, game.path.y+0.5);
		game.c.moveTo(game.two.x+0.5, game.two.y+0.5);
		game.c.lineTo(game.path.x+0.5, game.path.y+0.5);
		game.c.strokeStyle = color("pd");
		game.c.stroke();
		game.c.fillStyle = color("pd");
		game.c.font = "16px Arial";	
		game.c.fillText("Path Difference: "+Math.round(path_diff*10)/10+" wavelengths", game.path.x+10, game.path.y+7);
	}
	
	// Update
	if (game.mousedown && game.selected){
		if (game.selected == 1){
			game.one.y = mousePosition().y;
		}else if (game.selected == 2){
			game.two.y = mousePosition().y;
		}
	}else{
		game.selected = null;
	}
	
	// Time settings	
	var end = new Date().getTime();
	game.fps.dtime = Math.round(end - game.fps.last_tick);
	if (game.fps.dtime > 1000)
		game.fps.dtime = 1000;
	game.fps.last_tick = end;
}

$(init);
