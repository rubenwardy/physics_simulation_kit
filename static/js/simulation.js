

//
// Themes
//
var MODE = 0;
function color(name)
{
	return colors[name][MODE];
}

// Dialog
var __diaid = 0;
var __curdiaid = -1;
function close_dialog(){
	$('#dialog').remove();
	__curdiaid = -1;
}
function hide_dialog(){
	$('#dialog').fadeOut();
	__curdiaid = -1;
}
function create_dialog(){
	__diaid = __diaid + 1;
	close_dialog();
	__curdiaid = __diaid;
	$('body').append('<div id="dialog"><div id="dialog_body">[dialog]</div></div>');
	return $('#dialog_body');
}
function show_dialog(msg){
	create_dialog().html(msg);
}
function show_dialog_and_close(msg){
	create_dialog().html(msg);
	var curid = __diaid;
	setTimeout(function(){
		if (__diaid == curid)
			hide_dialog();
	}, 2000);
}
function dialog_content(){
	var d = $("#dialog");

	if (!d)
		return "";

	return d.html();
}

$(function() {
	$("#close_help").click(function() {
		$("#help").fadeOut();
	});

	$("#toggletheme").click(function() {
		if (MODE == 1) {
			console.log("Switching to dark mode");
			$("#toggletheme").text("Light Mode");
			$("body").removeClass("light_mode");
			MODE = 0;
		} else {
			console.log("Switching to light mode");
			$("#toggletheme").text("Dark Mode");
			$("body").addClass("light_mode");
			MODE = 1;
		}
	});
});
