Physics Simulation Kit
======================

A collection of graphical simulators used to demonstrate physics phenomenon.

* Standing Waves (standing.html)
* Interference Patterns (interference.html)
* Hooke's Law (hookes_law.html)
* Doppler Effect (doppler.html)
* Strings (strings.html)
* Psychedelic Doppler Effect (psychedelic.html)


Installation
------------

Download the latest version of this project at http://github.com/rubenwardy/physics_simulation_kit/zipball/master

Unzip the file that is downloaded.
Run a simulator by opening the correct file in a web browser.
The web browser MUST support HTML5 canvases.

License
-------

Created by rubenwardy.

License: GPLv3+
