var ROPE_LENGTH = 0.2; // the equilibirum length of each rope
var ROPE_LENGTH_SQ = ROPE_LENGTH * ROPE_LENGTH; // square of the rope length
var ROPE_SPACING = 0.2; // initial horizontal distance between vertices
var ROPE_MASS = 0.0005; // Mass per length of rope (actually the mass of vertices)
var ROPE_STRING_CONST = 0.5;
var STRING_VERTEX_DAMPING = 0.7;

points = [];
ropes = [];

function getRopes(id) {
	var retval = [];
	for (var i = 0; i < ropes.length; i++){
		if (ropes[i].one == id || ropes[i].two == id)
			retval.push({left: ropes[i].one == id, rope: ropes[i]});
	}
	return retval;
}

function createVertex(x, y, fixed) {
	points.push({ x: x, y: y, v: {x: 0, y: 0}, mass: ROPE_MASS, fixed: fixed});
}

function joinVertices(one, two) {
	ropes.push({ one: one, two: two, diff: {x: 0, y: 0}, dist: ROPE_LENGTH_SQ, strain: 0, spring_const: ROPE_STRING_CONST});
}

