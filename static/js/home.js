"use strict";

Array.prototype.groupBy = function(key) {
    return this.reduce(function(rv, x) {
        (rv[x[key]] = rv[x[key]] || []).push(x);
        return rv;
    }, {});
};

function simToHTML(sim) {
    const img = sim.thumbnail ? `<img src="${sim.thumbnail}">` : "";
    const gradient = "";
    // "background: linear-gradient(to right, #654ea3, #eaafc8);";
    return `<li>
            <a class="sim" href="${sim.url}">
                <div class="image" style="${gradient}">
                    ${img}
                </div>
                <h3 class="sim_title">${sim.title}</h3>
                <p class="sim_desc">
                    ${sim.description || ''}
                </p>
            </a>
        </li>`;
}

function setSimList(list, categories) {
    const category_lookup = buildCategoryLookup(categories);
    const boxer = x => category_lookup[x.name] || "Unsorted";

    const sections = list.map(x => {
        x.category = boxer(x);
        return x;
    }).groupBy("category");

    const content = Object.keys(sections).map(name => {
        const sectionBody = sections[name]
            .filter(sim => !sim.hidden)
            .map(simToHTML).join("");
        return `<li>
                <h2>${name}</h2>
                <ul class="sim_container">${sectionBody}</ul>
            </li>`;
    }).join("");

    const container = document.getElementById("sections");
    container.innerHTML = content;
}

function buildCategoryLookup(categories) {
    let lookup = {};
    Object.keys(categories).forEach(category => {
        categories[category]
            .forEach(sim => lookup[sim] = category);
    });

    return lookup;
}

setSimList(all_sims, {
    "Mechanics and Motion": ["projectile"],
    "Materials": ["hookes_law", "oscillation", "strings"],
    "Space": ["gravity"],
    "Electro-Magnetism": ["interference", "standing", "doppler", "fields"],
});

document.getElementById("loading").style.display = "none";
