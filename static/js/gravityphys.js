var xE5toPixel = 610 / 350;
var UGC = 66.73; // Universal Gravity Constant

function ForceSolver(mass)
{
	this.x = 0;
	this.y = 0;
	this.mass = mass;
}

ForceSolver.prototype.solve = function(dtime, gravity)
{
	if (gravity)
		this.y += 9.81 * this.mass;
	return {x: (this.x / this.mass) * dtime, y: (this.y / this.mass) * dtime};
};

ForceSolver.prototype.add = function(x, y)
{
	this.x += x;
	this.y += y;
}

ForceSolver.prototype.gravity = function(x1, y1, x2, y2, mass, r1, r2)
{
	var distsq = Math.pow((x1 - x2), 2) + Math.pow((y1 - y2), 2);
	if (distsq < 0.01)
		distsq = 0.01;

	var radiussq = Math.pow(r1 + r2, 2);
	var force = UGC * this.mass * mass / distsq;
	if (distsq < radiussq) {
		force = -force;// * (2 * (distsq / radiussq) - 1);
	}
	var angle = Math.atan2(y2 - y1, x2 - x1);
	this.add(force * Math.cos(angle), force * Math.sin(angle));
	return (distsq < radiussq);
}

function GravityPhys(collide_edge)
{
	this.collide_edge       = collide_edge;
	this.objects            = [];
	this.spawn_count        = 0;
	this.combine_on_collide = false;
	this.color_on_collide   = false;
	this.ftl                = 0;
	this.max_vel            = 0;
	this.sum_vel            = 0;
}

GravityPhys.prototype.autoSetScale = function(ce)
{
	xE5toPixel = ce.height / 400;
}

GravityPhys.prototype.newObject = (function() {
	function move_obj(dtime, w, h)
	{
		var to = {
			x: this.x + this.v.x * dtime,
			y: this.y + this.v.y * dtime
		};

		// mx - mx0 + y0
		var m = (to.y - this.y) / (to.x - this.x);


		// Left
		var left = - w / (2 * xE5toPixel);
		var collide = {
			x: left,
			y: m * left - m * this.x + this.y
		};
		if (
			(to.y > this.y && collide.y <= to.y && collide.y >= this.y) ||
			(to.y <= this.y && collide.y >= to.y && collide.y <= this.y)
		) {
			this.x = left + 0.1;
			this.y = collide.y;
			this.v.x *= -0.7;
			return;
		}

		// Right
		var left = w / (2 * xE5toPixel);
		var collide = {
			x: left,
			y: m * left - m * this.x + this.y
		};
		if (
			(to.y > this.y && collide.y <= to.y && collide.y >= this.y) ||
			(to.y <= this.y && collide.y >= to.y && collide.y <= this.y)
		) {
			this.x = left - 0.1;
			this.y = collide.y;
			this.v.x *= -0.7;
			return;
		}

		// Up
		var left = - h / (2 * xE5toPixel);
		var collide = {
			x: (left - this.y + m * this.x) / m,
			y: left
		};
		if (
			(to.y > this.y && collide.y <= to.y && collide.y >= this.y) ||
			(to.y <= this.y && collide.y >= to.y && collide.y <= this.y)
		) {
			this.x = collide.x;
			this.y = left + 0.1;
			this.v.y *= -0.7;
			return;
		}

		// Bottom
		var left = h / (2 * xE5toPixel);
		var collide = {
			x: (left - this.y + m * this.x) / m,
			y: left
		};
		if (
			(to.y > this.y && collide.y <= to.y && collide.y >= this.y) ||
			(to.y <= this.y && collide.y >= to.y && collide.y <= this.y)
		) {
			this.x = collide.x;
			this.y = left - 0.1;
			this.v.y *= -0.7;
			return;
		}

		this.x = to.x;
		this.y = to.y;
	}

	function speedsq_obj()
	{
		return Math.pow(this.v.x, 2) + Math.pow(this.v.y, 2)
	}

	function mass_obj()
	{
		var vsq = this.speedsq()
		var vsqbycsq = 1 - (vsq / 9000000);
		if (vsqbycsq < 0.001)
			vsqbycsq = 0.001;
		return this._mass / Math.sqrt(vsqbycsq);
	}

	return (function(pos, radius, mass, colors, v)
	{
		console.log("Spawning object at " + pos.toString());
		var tmp = {
			x: pos.x, y: pos.y,
			radius: radius,
			_mass: mass,
			mass: mass_obj,
			colors: colors,
			fixed: false,
			v: v ? {x:v.x, y:v.y} : {x:0,y:0},
			move: move_obj,
			speedsq: speedsq_obj
		};
		this.objects.push(tmp);
		return tmp;
	});
})();

GravityPhys.prototype.spawnBelt = function()
{
	for (var i = 0; i < 100; i++) {
		world.objects.push(newObj((Math.random() - 0.5) * 1000, (Math.random() - 0.5) * 600,
		5, 700));
	}
}

GravityPhys.prototype.radiusFromMass = function(mass)
{
	if (mass < 5)
		return (17 / 7);
	return mass * (17 / 70); // mass * (5 / 200)
}

GravityPhys.prototype.update = function(ce, dtime)
{
	for (var i = 0; i < world.objects.length; i++) {
		var obj = world.objects[i];
		if (!obj.fixed) {
			var f = new ForceSolver(obj.mass());
			for (var j = 0; j < world.objects.length; j++) {
				var obj2 = world.objects[j];
				if (i != j)
					if (f.gravity(obj.x, obj.y, obj2.x, obj2.y, obj2.mass(), obj.radius, obj2.radius)) {
						obj.is_in = true;
						if (world.combine_on_collide) {
							world.objects.splice(i, 1);
							obj2.x = (obj.x * obj.mass() + obj2.x * obj2.mass()) / (obj.mass() + obj2.mass());
							obj2.y = (obj.y * obj.mass() + obj2.y * obj2.mass()) / (obj.mass() + obj2.mass());
							obj2._mass += obj._mass;
							obj2.radius = radiusFromMass(obj2._mass);
							i--;
							break;
						}
					}
			}
			if (obj.is_in && world.combine_on_collide)
				break;
			var r = f.solve(dtime, false);
			obj.v.x += r.x;
			obj.v.y += r.y;
			obj.move(dtime, ce.width, ce.height);
		}
	}
}

GravityPhys.prototype.draw = function(ce, c)
{
	this.ftl = 0;
	this.max_vel = 0;
	this.sum_vel = 0;
	for (var i = 0; i < this.objects.length; i++) {
		// Get Object and color
		var obj = this.objects[i];
		if (obj.colors)
			c.fillStyle = obj.colors[MODE];
		else
			c.fillStyle = color("object");

		// Override color if collision or FTL
		if (this.color_on_collide) {
			var vel = obj.speedsq();
			this.sum_vel += vel;
			if (vel > this.max_vel)
				this.max_vel = vel;

			if (vel > 9000000 && !obj.fixed) {
				c.fillStyle = "red";
				this.ftl += 1;
			} else if (obj.is_in)
				c.fillStyle = "yellow";
			obj.is_in = false;
		}

		// Draw Object
		c.beginPath();
		assert(!isNaN(obj.x));
		assert(!isNaN(obj.y));
		c.arc(obj.x * xE5toPixel + ce.width/2,
				obj.y * xE5toPixel + ce.height/2,
				obj.radius * xE5toPixel, 0, 2 * Math.PI, false);
		c.fill();
	}
};
