var game = {
	selected: -1,
	paused: false,
	drawmode: false
};

function isPaused() {
	return game.paused || game.drawmode;
}

// Start
function init(){
	for (var i=0; i<50; i++) {
		createVertex(ROPE_SPACING * i, 0, false);
	}
	points[0].fixed = true;
	points[points.length-1].fixed = true;
	for (var i=0; i<49; i++) {
		joinVertices(i, i + 1);
	}
	engine.onKeyDown = function(e) {
		if (e.keyCode == 70 && game.selected >= 0 ) {
			var pt = points[game.selected];
			if (pt) {
				pt.fixed = !pt.fixed;
			}
		}
		if (e.keyCode == 80)
			game.paused = !game.paused;
		if (e.keyCode == 68)
			game.drawmode = !game.drawmode;
	};
	engine.onMouseDown = function(e) {
		game.selected = -1;
		var nearest = -1;
		var near_dist = null;
		for (var i = 0; i < points.length; i++) {
			var pos = {x: 150 + points[i].x * POS_SCALE, y: 150 + points[i].y * POS_SCALE};
			var dist = sq(engine.mouse.x - pos.x) + sq(engine.mouse.y - pos.y);
			if (nearest==-1 || dist < near_dist){
				nearest = i;
				near_dist = dist;
			}
		}
		if (near_dist < 2500){
			game.selected = nearest;
		}
	};
	engine.onMouseUp = function(e) {
		game.selected = -1;
	};
}

function getRopeColor(rope) {
	var colors = ["white", "white", "#FCC", "#FBB", "red", "red", "purple"];
	//["black", "#660000", "#990000", "#BB0000", "#E00000", "red", "purple"];
	var idx = (rope.dist / ROPE_LENGTH_SQ) / 40;
	assert(rope.dist >= 0, "rope's distance is less than zero!");
	if (idx >= 1) {
		idx = 1;
	} else if (idx < 0) {
		idx = 0;
	}
	assert(idx >= 0 && idx <=1, "getRopeColor() - idx out of range");
	return colors[Math.floor(idx * colors.length)];
}

function tick(ce, c, dtime)
{
	c.fillStyle = "white";
	c.font = "12px Arial";
	if (!isPaused()) {
		for (var i = 0; i < 5; i++) {
			if (game.selected >= 0 && game.selected < points.length){
				var pt = points[game.selected];
				var mouse = mousePosition();
				var pos = screenToPos(mouse.x, mouse.y);
				pt.x = pos.x;
				pt.y = pos.y;
				pt.v = {x: 0, y: 0};
			}
			resolve(dtime/5);
			cleanRopes();
		}
	} else {
		if (game.drawmode) {
			var mouse = mousePosition();
			c.fillText("Drawing", 20, 25);
			if (game.selected >= 0) {
				var pt = points[game.selected];
				var pos = {x: 150 + pt.x * POS_SCALE, y: 150 + pt.y * POS_SCALE};				
				var dist = sq(mouse.x - pos.x) + sq(mouse.y - pos.y);
				if (dist > ROPE_LENGTH_SQ * POS_SCALE * POS_SCALE) {					
					var mpos = screenToPos(mouse.x, mouse.y);
					createVertex(mpos.x, mpos.y);
					joinVertices(game.selected, points.length - 1);
					game.selected = points.length - 1;
				}
			}
		} else {
			var mouse = mousePosition();
			c.fillText("Paused!", 20, 25);
			if (game.selected >= 0 && game.selected < points.length){
				var pt = points[game.selected];
				var pos = screenToPos(mouse.x, mouse.y);
				pt.x = pos.x;
				pt.y = pos.y;
				pt.v = {x: 0, y: 0};
			}

		}
	}
	
	for (var i = 0; i < ropes.length; i++) {
		var rope = ropes[i];
		var one = points[rope.one];
		var two = points[rope.two];
		var onep = posToScreen(one.x, one.y);
		var twop = posToScreen(two.x, two.y);
		c.beginPath();
		c.strokeStyle = getRopeColor(rope);
		c.moveTo(onep.x, onep.y);
		c.lineTo(twop.x, twop.y);
		c.stroke();
	}
	if (game.drawmode) {
		for (var i = 0; i < points.length; i++) {
			var pos = posToScreen(points[i].x, points[i].y);
			c.beginPath();
			c.arc(pos.x, pos.y, 3, 0, 2 * Math.PI, false);
			c.fillStyle = 'white';
			c.fill();
		}
	}
	if (game.selected>=0){
		action = "pin";
		if (points[game.selected].fixed)
			action = "unpin";
		c.fillText("Vertex "+game.selected+" selected! (F to "+action+")", 20, 40);
	}
}
