function vectorAdd(one, two) {
	return {x: one.x + two.x, y: one.y + two.y};
}

function vectorTake(one, two) {
	return {x: one.x - two.x, y: one.y - two.y};
}

function vectorMultVF(one, two) {
	return {x: one.x * two, y: one.y * two};
}

function to2dp(input) {
	return Math.round(input*100)/100;
}

var POS_SCALE = 75; // pixels in a metre
var X_OFFSET = 150;
var Y_OFFSET = 150;
function screenToPos(x, y){
	return {x: (x - X_OFFSET)/POS_SCALE, y: (y - Y_OFFSET)/POS_SCALE};
}
function posToScreen(x, y) {
	return {x: X_OFFSET + x * POS_SCALE, y: Y_OFFSET + y * POS_SCALE};
}

function sq(input) {
	return input * input;
}
