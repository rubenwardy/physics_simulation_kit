ROPE_LENGTH = 4; // the equilibirum length of each rope
ROPE_LENGTH_SQ = ROPE_LENGTH * ROPE_LENGTH; // square of the rope length
ROPE_SPACING = 0.2; // initial horizontal distance between vertices
ROPE_MASS = 5; // Mass per length of rope (actually the mass of vertices)
X_OFFSET = 100;
Y_OFFSET = 20;
POS_SCALE = 40;
CANVAS_H_MARGIN = 210;

var forcegraph = new Graph(
	{
		title: "Extension",
		name: "x",
		unit: "m",
		minval: 0,
		maxval: 17,
		step: 1
	},
	{
		title: "Force",
		name: "f",
		unit: "N",
		minval: 0,
		maxval: 10,
		step: 1
	}
);

var current_graph = 0;

// Start
function init(){
	createVertex(0, 0, false);
	createVertex(0, 4, false);
	points[0].fixed = true;
	joinVertices(0, 1);
	POS_SCALE = ($(window).height()-40) / 24;
	engine.onKeyDown = function(e) {
		if (e.keyCode == 38) {
			points[1].mass = Math.round((points[1].mass+0.1)*10)/10;
		} else if (e.keyCode == 40) {
			points[1].mass = Math.round((points[1].mass-0.1)*10)/10;
		}
	};
	$("#plot").click(function(){
		console.log("Plotted (" + (Math.sqrt(ropes[0].dist) - ROPE_LENGTH) + ", " + (points[1].mass * 9.81) + ")");
		forcegraph.plot({x: Math.sqrt(ropes[0].dist) - ROPE_LENGTH, f: points[1].mass * 9.81});
	});
	$("#random").click(function(){
		forcegraph = new Graph(
			{
				title: "Extension",
				name: "x",
				unit: "m",
				minval: 0,
				maxval: 17,
				step: 1

			},
			{
				title: "Force",
				name: "f",
				unit: "N",
				minval: 0,
				maxval: 10,
				step: 1
			}
		);
		ropes[0].spring_const = to2dp((Math.random())+0.5);
		console.log("springconstant is: " + ropes[0].spring_const);
		current_graph = 0;
		$("#post").remove();
	});
	$("#answer").click(function(){
		$("#post").remove();
		$("#bottom_panel").prepend("<span id=\"post\">String constant is " + ropes[0].spring_const + "<br /></span>");
	});
	current_graph = $("#y_against_x").val();
}

function tick(ce, c, dtime){
	var dd = $("#y_against_x").val();
	if (dd != current_graph) {
		current_graph = dd;
		if (dd==1)
			forcegraph.setAxis("x","f");
		else
			forcegraph.setAxis("f","x");
		forcegraph.generateLOBF();
	}

	// Limit dtime
	if (dtime > 0.2)
		dtime = 0.1;

	// User input
	var num = Number($('#weight').val());
	if (num > 0) {
		points[1].mass = num / 9.81;
		$("#warning").hide();
	} else {
		$("#warning").show();
	}

	// Do physics
	for (var i = 0; i < 10; i++) {
		resolve(dtime/2);
	}

	// Draw tension for debuging
	points[1].x = 0;
	points[1].v.x = 0;

	// Draw ropes
	for (var i = 0; i < ropes.length; i++) {
		var rope = ropes[i];
		var one = points[rope.one];
		var two = points[rope.two];
		var onep = posToScreen(one.x, one.y);
		var twop = posToScreen(two.x, two.y);
		c.beginPath();
		c.strokeStyle = "white";
		c.moveTo(onep.x+0.5, onep.y+0.5);
		c.lineTo(twop.x+0.5, twop.y+0.5);
		c.lineWidth = 3;
		c.stroke();
	}
	var pos = posToScreen(points[1].x, points[1].y);
	c.beginPath();
	c.arc(pos.x, pos.y + 10, 10, 0, 2 * Math.PI, false);
	c.fillStyle = "silver";
	c.fill();

	forcegraph.drawAxis(c, 200, 20, ce.width - 200, (ce.height-60));
}
