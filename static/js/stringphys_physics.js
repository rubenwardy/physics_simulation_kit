 function createForceSolver() {
	return {
		x: 0,
		y: 0,
		solve: function(dtime, mass, gravity) {
			if (gravity)
				this.y += 9.81 * mass;
			return {x: (this.x / mass) * dtime, y: (this.y / mass) * dtime};
		},
		add: function(x, y) {
			this.x += x;
			this.y += y;
		},
		addTension: function(x, y, lsq, springconst, isleft) {
			assert(springconst && typeof (springconst) != NaN, "SC is NaN "+springconst);

			// Distance and extension
			var dist = x*x + y*y;
			var ext = Math.sqrt(dist) - Math.sqrt(lsq);

			// Calculate force
			var force = springconst * ext;
			var angle = 0;
			if (isleft)
				angle =  Math.atan2(y, x);
			else
				angle = Math.atan2(y, x);

			// Validation
			assert(typeof (force) != NaN, "Force is NaN");
			assert(typeof (angle) != NaN, "Angle is NaN");

			// Apply force components
			var f = { x: Math.cos(angle) * force, y: Math.sin(angle) * force };
			this.add(f.x, f.y);

			// Debug outpu
			//console.log("(" + to2dp(x)  + ", " + to2dp(y) + ") ext: " + to2dp(ext) + ", force " + to2dp(force) + " at " + to2dp(angle*180/Math.PI) + " (" + to2dp(f.x) + ", " + to2dp(f.y) + ")");
		}
	};
}

var STRCON = 0.5;

function getAcceleration(i, dtime) {
	var pt = points[i];
	var forces = createForceSolver();
	var conropes = getRopes(i);
	for (var r = 0; r < conropes.length; r++) {
		var rope = conropes[r];
		if (rope.left)
			forces.addTension(-rope.rope.diff.x, -rope.rope.diff.y, ROPE_LENGTH_SQ, rope.rope.spring_const, true);
		else
			forces.addTension(rope.rope.diff.x, rope.rope.diff.y, ROPE_LENGTH_SQ, rope.rope.spring_const, false);
	}
	return forces.solve(dtime, pt.mass, true);
}

function resolve(dtime) {
	// Process ropes
	for (var i = 0; i < ropes.length; i++) {
		var rope = ropes[i];
		var diff = vectorTake(points[rope.one], points[rope.two]);
		var dist = diff.x*diff.x + diff.y*diff.y;
		rope.dist = dist;
		rope.diff = diff;
	}

	// Solve
	for (var i = 0; i < points.length; i++) {
		var pt = points[i];
		if (!pt.fixed) {
			var acc = getAcceleration(i, dtime);
			pt.v = vectorAdd(pt.v, acc);
			pt.v = vectorMultVF(pt.v, 1 - STRING_VERTEX_DAMPING * dtime);
			pt.x += pt.v.x * dtime;
			pt.y += pt.v.y * dtime;
		}
	}
}

// Remove any broken ropes
// WARNING: Is recursive.
function cleanRopes() {
	for (var i = 0; i < ropes.length; i++) {
		var rope = ropes[i];
		if (rope.dist/ROPE_LENGTH_SQ > 42) {
			rope.strain = rope.strain + 1;
			if (rope.strain > 50) {
				ropes.splice(i, 1);
				cleanRopes();
				return;
			}
		} else {
			rope.strain = 0;
		}
	}
}

