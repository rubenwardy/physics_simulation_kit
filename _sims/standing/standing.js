var game = {
	fps: {
		dtime: 0,
		last_tick: 0
	},
	time: 0
};

var colors = {
	wave: ["white", "black"]
};

// Start
function init()
{
	game.c = engine.c;
	game.ce = engine.ce;
}

function __heightAtPoint(x, w, f, o){
	return Math.sin((x/w*f + o)*2*Math.PI);
}
function heightAtPoint(x, y, w, h, f, o){
	return __heightAtPoint(x, w, f, o) * h/2 + y + 0.5;
}
function sumAtPoint(x, y, w, h, f, o){
	return (__heightAtPoint(x, w, f, o) + __heightAtPoint(x, w, f, 1-o)) / 2 * h/2 + y + 0.5; 
}
function drawSineWave(x, y, w, h, f, o, eq){
	game.c.beginPath();
	game.c.strokeStyle = color("wave");
	game.c.moveTo(x + 0.5, eq(x, y, w, h, f, o));
	for (ix = 1; ix < w; ix++){
		game.c.lineTo(x + ix + 0.5, eq(x + ix, y, w, h, f, o));
	}	
	game.c.stroke();
}
function tick(ce, c, dtime)
{
	// Get properties
	var frequency = 1;
	var wavelength = 2;
	
	if ($("#mode").val()==2){
		frequency = Number($('#fund_speed').val());
		wavelength = Number($('#fundament').val()) / 2;
		$("#advanced").hide();
		$("#fund").show();
		$("#fund_data").html("&lambda; " + Math.round(1/wavelength * 10000)/10000);
	}else{
		frequency = Number($('#speed').val());
		wavelength = Number($('#wavelength').val());
		$("#advanced").show();
		$("#fund").hide();
		$("#adv_data").html("&lambda; " + Math.round(1/wavelength * 10000)/10000);
	}
	
	// Advance time
	game.time = (game.time + dtime * 0.5 * frequency) % 1;	
	
	// Draw graphs
	var third = $(window).height()/3;
	drawSineWave(20,third/2,$(window).width() - 240,120,wavelength,game.time,heightAtPoint);
	drawSineWave(20,third + third/2,$(window).width() - 240,120,wavelength,1-game.time,heightAtPoint);
	drawSineWave(20,2*third + third/2,$(window).width() - 240,120,wavelength,game.time,sumAtPoint);
	
	// Finish tick	
	var end = new Date().getTime();
	game.fps.dtime = Math.round(end - game.fps.last_tick);
	if (game.fps.dtime > 1000)
		game.fps.dtime = 1000;
	game.fps.last_tick = end;
}